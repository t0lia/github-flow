# GHF

This is a POC project to demonstrate github flow implementation for java project.

## Gitlab CI setup
- key generation: `ssh-keygen -m PEM -t rsa -P "" -f deploy_key`

## Docker
1. run separate step from pipeline
`docker run --name github-flow -v $(pwd):/builds/t0lia/github-flow -it --entrypoint bash maven:3.8.6-amazoncorretto-17`
2. build an image
`docker build -t t0lia/github-flow .`

## Gitlab CI documentation
1. [job control](https://docs.gitlab.com/ee/ci/jobs/job_control.html)
2. [caching](https://docs.gitlab.com/ee/ci/caching/)

## Differences between workflows
1. [gihflow](https://nvie.com/posts/a-successful-git-branching-model/)
2. [gihubflow](https://githubflow.github.io/)
3. [gitlabflow](https://docs.gitlab.com/ee/topics/gitlab_flow.html)
4. [trunk-based](https://paulhammant.com/2013/04/05/what-is-trunk-based-development/)

## Deploy techniques
1. [Etsy](https://www.networkworld.com/article/2886672/how-etsy-makes-devops-work.html)
2. [Netflix](https://www.infoq.com/news/2013/06/netflix/)
3. [Uber](https://www.infoq.com/articles/uber-deployment-planet-scale/)
4. [Airbnb](https://www.altoros.com/blog/airbnb-deploys-125000-times-per-year-with-multicluster-kubernetes/)
5. [Github](https://github.blog/2021-02-03-deployment-reliability-at-github/)
6. [Asana](https://blog.asana.com/2013/02/onboarding-new-engineers/)
7. [Intercom](https://www.intercom.com/blog/from-zero-to-shipping-code/)