package com.apozdniakov.githubflow.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    private final String applicationName;
    private final String applicationVersion;
    private final String applicationDescription;

    public HomeController(
            @Value("${app.name}") String applicationName,
            @Value("${app.version}") String applicationVersion,
            @Value("${app.description}") String applicationDescription
    ) {
        this.applicationName = applicationName;
        this.applicationVersion = applicationVersion;
        this.applicationDescription = applicationDescription;
    }

    @GetMapping
    public String index() {
        return String.format("name: %s, version: %s, description: %s",
                applicationName, applicationVersion, applicationDescription);
    }
}
