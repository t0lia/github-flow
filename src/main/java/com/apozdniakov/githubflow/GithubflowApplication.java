package com.apozdniakov.githubflow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GithubflowApplication {

	public static void main(String[] args) {
		SpringApplication.run(GithubflowApplication.class, args);
	}

}
