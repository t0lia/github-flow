package com.apozdniakov.githubflow.controller;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HomeControllerTest {

    @Test
    public void checkIndex() {
        HomeController homeController = new HomeController("githubflow", "1.0.0", "githubflow");
        String result = homeController.index();
        assertEquals("name: githubflow, version: 1.0.0, description: githubflow", result);
    }

}