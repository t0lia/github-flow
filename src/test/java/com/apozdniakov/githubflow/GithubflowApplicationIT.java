package com.apozdniakov.githubflow;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class GithubflowApplicationIT {

    @Autowired
    TestRestTemplate testRestTemplate;

    @Test
    void testRestApi() {
        ResponseEntity<String> response = testRestTemplate.getForEntity("/", String.class);
        assertEquals(200, response.getStatusCodeValue());
        Assertions.assertTrue(response.getBody().startsWith("name: github-flow"));
    }

}