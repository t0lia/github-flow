# build backend
FROM maven:3.8.6-amazoncorretto-17 AS MAVEN_TOOL_CHAIN

COPY ./ /tmp/

WORKDIR /tmp/

RUN mvn clean verify -P full

# production
FROM amazoncorretto:17

EXPOSE 8080

RUN mkdir /app

COPY --from=MAVEN_TOOL_CHAIN /tmp/target/*.jar /app/app.jar

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app/app.jar"]
